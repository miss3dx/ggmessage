const route = require("color-convert/route");
const express = require("express");
const body_parser = require("body-parser");

global.mongoose = require("mongoose");
mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connection.on("connected", ()=>{
    console.log("[Main] Connected to database.");
});

global.fs = require("fs");

global.app = express();
app.set("view engine", "ejs");
app.use(body_parser.json());

fs.readdirSync(__dirname + "/routes/").forEach(routefile=>{
    app.use(require(`${__dirname}/routes/${routefile}`));
});

app.listen(process.env.MAIN_PORT, ()=>{
    console.log(`[Main] Running on port ${process.env.MAIN_PORT}.`);
});