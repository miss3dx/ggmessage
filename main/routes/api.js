const express = require("express");
const { RLHandler, gentoken } = require("../../util.js");

const router = express.Router();

router.RLHandler = new RLHandler;

const usermodel = require("../models/user.js");
const dmmodel = require("../models/dm.js");
const groupmodel = require("../models/group.js");
const groupmembermodel = require("../models/groupmember.js");
const gmmodel = require("../models/gm.js");
const gmreadmodel = require("../models/gmread.js");
const groupmember = require("../models/groupmember.js");

router.post("/api/users/edit", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let body = req.body;
    if(body.password && (typeof(body.password) != "string"?true:(body.password.length <= 0 || body.password.length >= 33))){
        return res.json({error: true, message: "Invalid password."});
    }
    if(body.mail && (typeof(body.mail) != "string"?true:(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(body.mail.toLowerCase())))){
        return res.json({error: true, message: "Invalid mail."});
    }
    if(body.username && (typeof(body.username != "string"?true:(body.username.length <= 0 || body.username.length >= 25)))){
        return res.json({error: true, message: "Invalid username."});
    }
    if(body.custom_username && (typeof(body.custom_username != "string"?true:(body.custom_username.length <= 0 || body.custom_username.length >= 25)))){
        return res.json({error: true, message: "Invalid custom username."});
    }
    let prev_mail = user.mail, prev_username = user.username, prev_password = user.password, prev_custom_username = user.custom_username;
    if(body.mail){
        user.mail = body.mail;
        if((await usermodel.findOne({mail: body.mail}))){res.status(403); return res.json({error: true, message: "Mail is taken."});}
    }
    if(body.username){
        user.username = body.username;
        if((await usermodel.findOne({username: body.username}))){res.status(403); return res.json({error: true, message: "Username is taken."});}
    }
    if(body.custom_username)user.custom_username = body.custom_username;
    if(body.password)user.password = require("crypto").Hash("sha256").update(body.password).digest("hex");
    if(prev_mail == user.mail && prev_username == user.username && prev_password == user.password && prev_custom_username == user.custom_username){return res.json({error: false, message: "Nothing changed."});}
    if(!router.RLHandler.push("user_edit", user.user_id, 1, 300000)){res.status(403);return res.json({error: true, message: "Ratelimited."});}
    await user.save();
    return res.json({error: false, message: "Success."});
});

router.get("/api/users/*", async(req, res)=>{
    if(req.url.slice(11) == "edit")return;
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let userid = req.url.slice(11);
    if(!userid){res.status(400);return res.json({error: true, message: "User ID not specified."});}
    let target = await usermodel.findOne({user_id: (userid == "me" ? user.id : userid)});
    if(!target){res.status(404);return res.json({error: true, message: "Not Found."});}
    let result = {
        error: false,
        user_id: target.user_id,
        username: target.username,
        tag: target.tag,
        register_timestamp: target.register_timestamp,
        avatar: target.avatar,
        badges: target.badges
    };
    if(user.id == target.id){
        result.status = target.status;
        result.mail = target.mail;
    }
    return res.json(result);
});

router.post("/api/dms/*/send", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let user_id = req.url.slice(9).split("/")[0];
    if(!user_id){res.status(400);return res.json({error: true, message: "User ID not specified."});}
    let target = await usermodel.findOne({user_id: (user_id == "me" ? user.id : user_id)});
    if(!target){res.status(404);return res.json({error: true, message: "Not Found."});}
    let content = req.body.content;
    if(req.body.file_urls){
        if(!Array.isArray(req.body.file_urls)){res.status(400); return res.json({error: true, message: "Invalid file_urls."});}
        for(let u in req.body.file_urls){
            if(!(typeof(u) == "string")){res.status(400); return res.json({error: true, message: "Invalid file_urls."});}
            if(!u.startsWith(process.env.FILEHOST_URL+(process.env.FILEHOST_PORT == 80 ? "" : ":" + process.env.FILEHOST_PORT))){return res.json({error: true, message: "Invalid file_urls."});}
        }
    }
    if(!content){res.status(400); return res.json({error: true, message: "Invalid or no content."});}
    let dm = new dmmodel({
        created_timestamp: Date.now(),
        sender_id: user.id,
        receiver_id: target.id,
        content,
        file_urls: req.body.file_urls || []
    });
    if(!router.RLHandler.push("dm_send", user.user_id, 120)){res.status(403);return res.json({error: true, message: "Ratelimited."});}
    dm.message_id = dm._id.toString();
    await dm.save();
    return res.json({error: false, message_id: dm.message_id});
});

router.get("/api/messages/*", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let messageid = req.url.slice(14);
    if(!messageid){res.status(400);return res.json({error: true, message: "Message ID not specified."});}
    let dm = await dmmodel.findOne({message_id: messageid});
    if(!dm){res.status(404);return res.json({error: true, message: "Not Found."});}
    let result = {
        error: false,
        message_id: dm.message_id,
        created_timestamp: dm.created_timestamp,
        sender_id: dm.sender_id,
        receiver_id: dm.receiver_id,
        content: dm.content,
        file_urls: dm.file_urls,
        deleted: dm.deleted
    };
    return res.json(result);
});

router.get("/api/getUpdates", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let dms = await dmmodel.find({unread: true});
    let final = [];
    for(let i in dms){
        let dm = dms[i];
        final.push({
            type: "dm",
            message_id: dm.message_id,
            created_timestamp: dm.created_timestamp,
            sender_id: dm.sender_id,
            receiver_id: dm.receiver_id,
            content: dm.content,
            file_urls: dm.file_urls,
            deleted: dm.deleted
        });
        await dmmodel.updateOne({_id: dm.id}, {unread: false});
    }
    let group_ids = (await groupmember.find({user_id: user.id})).map(gmember => gmember.group_id);
    for(let i in group_ids){
        let gms = await gmmodel.find({
            group_id: group_ids[i]
        });
        for(let j in gms){
            let gm = gms[j];
            if((await gmreadmodel.findOne({message_id: gm.id, user_id: user.id})))continue;
            final.push({
                type: "gm",
                message_id: gm.message_id,
                created_timestamp: gm.created_timestamp,
                sender_id: gm.sender_id,
                group_id: gm.group_id,
                content: gm.content,
                file_urls: gm.file_urls,
                deleted: gm.deleted
            });
            await ((new gmreadmodel({message_id: gm.id, user_id: user.id})).save)();
        }
    }
    return res.json(final);
});

router.post("/api/groups/create", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let body = req.body;
    if(typeof(body.group_custom_name) == "string" && typeof(body.private) == "boolean" && (!body.private?typeof(body.group_name)=="string":true) && !(body.group_custom_name.length <= 0 || body.group_custom_name.length >= 32) && !(body.group_name && (body.group_name.length <= 0 || body.group_name.length >= 24))){
        let data = {
            created_timestamp: Date.now(),
            owner_id: user.user_id,
            private: body.private,
            group_custom_name: body.group_custom_name
        };
        if(!body.private){
            data.group_name = body.group_name;
            if((await groupmodel.findOne({group_name: data.group_name}))){res.status(403);return res.json({error: true, message: "group_name is taken."});}
        }else{
            data.group_secret = require("crypto").Hash("sha256").update(data.group_custom_name + "." + data.owner_id + "." + data.created_timestamp + "." + require("crypto").randomBytes(20).toString("hex")).digest("hex");
        }
        let group = new groupmodel(data);
        if(!router.RLHandler.push("group_create", user.user_id, 2, 1200000)){res.status(403);return res.json({error: true, message: "Ratelimited."});}
        group.group_id = group._id.toString();
        await group.save();
        let joindata = {
            user_id: user.id,
            group_id: group.id
        };
        await (new groupmembermodel(joindata)).save();
        let result = {
            error: false,
            group_id: group.group_id
        };
        if(group.private){
            result.group_secret = group.group_secret;
        }
        return res.json(result);
    }else{res.status(400);return res.json({error: true, message: "Invalid payload."});}
});

router.post("/api/group_join/*", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let group_inv = req.url.slice(16);
    if(!group_inv){res.status(400); return res.json({error: true, message: "Group invite not specified."});}
    let group = await groupmodel.findOne({
        group_id: group_inv
    });
    if(!group){
        group = await groupmodel.findOne({
            group_secret: group_inv
        });
    }
    if(!group){
        group = await groupmodel.findOne({
            group_custom_name: group_inv
        });
    }
    if(!group){res.status(404); return res.json({error: true, message: "Not Found."});}
    if(group.private && group.id == group_inv){res.status(404); return res.json({error: true, message: "Not Found."});}
    if(!router.RLHandler.push("group_join", user.id, 15)){res.status(403); return res.json({error: true, message: "Ratelimited."});}
    let data = {
        user_id: user.id,
        group_id: group.id
    };
    let check = await groupmembermodel.findOne(data);
    if(check){
        res.status(403);
        if(check.state == -1){return res.json({error: true, message: "You are banned from this group."});}
        return res.json({error: true, message: "You are already in group."});
    }
    let groupmember = new groupmembermodel(data);
    await groupmember.save();
    return res.json({error: false, group_id: group.id, group_custom_name: group.group_custom_name});
});

router.post("/api/groups/delete/*", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let group_id = req.url.slice(19);
    if(!group_id){res.status(400); return res.json({error: true, message: "Group ID not specified."});}
    let group = await groupmodel.findOne({group_id});
    if(!group){res.status(404); return res.json({error: true, message: "Not Found."});}
    let groupmember = await groupmembermodel.findOne({user_id: user.id, group_id: group.group_id});
    if(!groupmember || (groupmember && groupmember.state == -1)){res.status(403); return res.json({error: true, message: "You are not in group."});}
    if(groupmember.state != 2){res.status(403); return res.json({error: true, message: "Only owners can perform this action."});}
    if(!router.RLHandler.push("group_delete", user.user_id, 1, 1800000)){res.status(403); return res.json({error: true, message: "Ratelimited."});}
    await groupmembermodel.remove({group_id: group.group_id});
    await groupmodel.deleteOne({group_id: group.group_id});
    return res.json({error: false, message: "Success."});
});

router.post("/api/groups/ban/*/*", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let group_id = req.url.slice(16).split("/")[0];
    if(!group_id){res.status(400); return res.json({error: true, message: "Group ID not specified."});}
    let group = await groupmodel.findOne({group_id});
    if(!group){res.status(404); return res.json({error: true, message: "Not Found."});}
    let groupmember = await groupmembermodel.findOne({user_id: user.id, group_id: group.group_id});
    if(!groupmember || (groupmember && groupmember.state == -1)){res.status(403); return res.json({error: true, message: "You are not in group."});}
    if(groupmember.state < 1){res.status(403); return res.json({error: true, message: "Only owners and administrators can perform this action."});}
    let target_id = req.url.slice(16).split("/")[1];
    if(!target_id){res.status(400); return res.json({error: true, message: "Target ID not specified."});}
    let target = await usermodel.findOne({user_id: target_id});
    if(!target){res.status(404); return res.json({error: true, message: "Target Not Found."});}
    let target_groupmember = await groupmembermodel.findOne({user_id: target.id, group_id: group.group_id});
    if(!target_groupmember || (target_groupmember && target_groupmember.state == -1)){res.status(403); return res.json({error: true, message: "Target member is not in group or already banned."});}
    if(groupmember.state != 2 && target_groupmember.state >= 1){res.status(403); return res.json({error: true, message: "You cannot ban owner or administators."});}
    target_groupmember.state = -1;
    await groupmembermodel.deleteOne({user_id: target.id, group_id: group.id});
    return res.json({error: true, message: "Success."});
});

router.post("/api/groups/setadminstate/*/*/*", async(req,res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let group_id = req.url.slice(26).split("/")[0];
    if(!group_id){res.status(400); return res.json({error: true, message: "Group ID not specified."});}
    let group = await groupmodel.findOne({group_id});
    if(!group){res.status(404); return res.json({error: true, message: "Not Found."});}
    let groupmember = await groupmembermodel.findOne({user_id: user.id, group_id: group.group_id});
    if(!groupmember || (groupmember && groupmember.state == -1)){res.status(403); return res.json({error: true, message: "You are not in group."});}
    if(groupmember.state != 2){res.status(403); return res.json({error: true, message: "Only owners can perform this action."});}
    let target_id = req.url.slice(26).split("/")[1];
    if(!target_id){res.status(400); return res.json({error: true, message: "Target ID not specified."});}
    let target = await usermodel.findOne({user_id: target_id});
    if(!target){res.status(404); return res.json({error: true, message: "Target Not Found."});}
    let target_groupmember = await groupmembermodel.findOne({user_id: target.id, group_id: group.group_id});
    if(!target_groupmember || (target_groupmember && target_groupmember.state == -1)){res.status(403); return res.json({error: true, message: "Target member is not in group or already banned."});}
    if(groupmember.state != 2 && target_groupmember.state == 2){res.status(403); return res.json({error: true, message: "You cannot change state of yourself."});}
    let state = req.url.slice(26).split("/")[1];
    if(state != "1" && state != "2"){res.status(400); return res.json({error: true, message: "Invalid payload."});}
    state = Number(state);
    if(!router.RLHandler.push("group_setadminstate", user.id, 10, 240000)){res.status(403); return res.json({error: true, message: ""});}
    target_groupmember.state = state;
    await target_groupmember.save();
    return res.json({error: false, message: "Success."});
});

router.post("/api/group_messages/*/send", async(req, res)=>{
    if(!req.headers["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.headers.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    let group_id = req.url.slice(20).split("/")[0];
    if(!group_id){res.status(400); return res.json({error: true, message: "Group ID not specified."});}
    let group = await groupmodel.findOne({group_id});
    if(!group){res.status(404); return res.json({error: true, message: "Not Found."});}
    let groupmember = await groupmembermodel.findOne({user_id: user.id, group_id: group.group_id});
    if(!groupmember || (groupmember && groupmember.state == -1)){res.status(403); return res.json({error: true, message: "You are not in group."});}
    let content = req.body.content;
    if(req.body.file_urls){
        if(!Array.isArray(req.body.file_urls)){res.status(400); return res.json({error: true, message: "Invalid file_urls."});}
        for(let u in req.body.file_urls){
            if(!(typeof(u) == "string")){res.status(400); return res.json({error: true, message: "Invalid file_urls."});}
            if(!u.startsWith(process.env.FILEHOST_URL+(process.env.FILEHOST_PORT == 80 ? "" : ":" + process.env.FILEHOST_PORT))){return res.json({error: true, message: "Invalid file_urls."});}
        }
    }
    if(!content){res.status(400); return res.json({error: true, message: "Invalid or no content."});}
    let gm = new gmmodel({
        created_timestamp: Date.now(),
        sender_id: user.id,
        group_id: group.id,
        content,
        file_urls: req.body.file_urls || []
    });
    if(!router.RLHandler.push("gm_send", user.user_id, 120)){res.status(403);return res.json({error: true, message: "Ratelimited."});}
    gm.message_id = gm._id.toString();
    await gm.save();
    return res.json({error: false, message_id: gm.message_id});
});

router.post("/api/login", async(req, res)=>{
    let body = req.body;
    if(typeof(body.mail) != "string" || typeof(body.password) != "string"){res.status(400); return res.json({error: true, message: "Invalid payload."});}
    if(!router.RLHandler.push("login", (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim(), 2, 300000)){res.status(400); return res.json({error: true, message: "Ratelimited."});}
    let user = await usermodel.findOne({
        mail: body.mail,
        password: require("crypto").Hash("sha256").update(body.password).digest("hex")
    });
    if(!user){res.status(404); return res.json({error: true, message: "Not Found."});}
    return res.json({error: false, token: user.token});
});

router.post("/api/register", async(req, res)=>{
    let body = req.body;
    if(typeof(body.mail) != "string" || typeof(body.password) != "string" || typeof(body.username) != "string" || typeof(body.custom_username) != "string"){res.status(400); return res.json({error: true, message: "Invalid payload."});}
    if(body.password.length <= 0 || body.password.length >= 33){
        return res.json({error: true, message: "Invalid password."});
    }
    if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(body.mail.toLowerCase())){
        return res.json({error: true, message: "Invalid mail."});
    }
    if(body.username.length <= 0 || body.username.length >= 25){
        return res.json({error: true, message: "Invalid username."});
    }
    if(body.custom_username.length <= 0 || body.custom_username.length >= 25){
        return res.json({error: true, message: "Invalid custom username."});
    }
    if((await usermodel.findOne({username: body.username}))){res.status(403); return res.json({error: true, message: "Username is taken."});}
    if((await usermodel.findOne({mail: body.mail}))){res.status(403); return res.json({error: true, message: "Mail is taken."});}
    if(!router.RLHandler.push("register", (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim(), 1, 3600000)){res.status(403); return res.json({error: true, message: "Ratelimited."});}
    let timestamp = Date.now();
    let user = new usermodel({
        mail: body.mail,
        username: body.username,
        custom_username: body.custom_username,
        password: require("crypto").Hash("sha256").update(body.password).digest("hex"),
    });
    user.user_id = user._id.toString();
    user.token = gentoken(body.username, user.user_id, timestamp);
    await user.save();
    return res.json({error: false, token: user.token});
});

module.exports = router;