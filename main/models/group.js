const schema = mongoose.Schema({
    group_id: String,
    created_timestamp: String,
    owner_id: String,
    private: Boolean,
    group_name: String,
    group_custom_name: String,
    group_secret: String,
});

module.exports = mongoose.model("Group", schema);