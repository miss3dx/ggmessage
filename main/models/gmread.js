const schema = mongoose.Schema({
    message_id: String,
    user_id: String
});

module.exports = mongoose.model("GMRead", schema);