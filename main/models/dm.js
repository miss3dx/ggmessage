const schema = mongoose.Schema({
    message_id: String,
    created_timestamp: String,
    sender_id: String,
    receiver_id: String,
    content: String,
    file_urls: Array,
    unread: {type: Boolean, default: true},
    deleted: {type: Boolean, default: false}
});

module.exports = mongoose.model("DM", schema);