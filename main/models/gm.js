const schema = mongoose.Schema({
    message_id: String,
    created_timestamp: String,
    sender_id: String,
    group_id: String,
    content: String,
    file_urls: Array,
    deleted: {type: Boolean, default: false}
});

module.exports = mongoose.model("GM", schema);