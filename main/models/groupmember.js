const schema = mongoose.Schema({
    user_id: String,
    group_id: String,
    state: {type: Number, default: 0}
});

module.exports = mongoose.model("GroupMember", schema);