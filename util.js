module.exports = {
    makeid: function(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
    },
    gentoken(username, userid, register_timestamp){
        return require("crypto").Hash("sha256").update(module.exports.makeid(24)+"."+Buffer.from(username+"."+userid).toString("base64")+"."+Buffer.from(register_timestamp.toString()).toString("base64")).digest("hex");
    },
    RLHandler: class RLHandler {
        constructor(){
            this.data = {};
        }
        push(taskname, userid, limit, limtime){
            if(!limtime)limtime = 60000;
            if(!this.data[taskname])this.data[taskname] = {};
            if(!this.data[taskname][userid]){this.data[taskname][userid] = [Date.now()]; return true;}
            let now = Date.now();
            this.data[taskname][userid] = this.data[taskname][userid].filter(task => task >= now - limtime);
            if(this.data[taskname][userid].length >= limit)return false;
            this.data[taskname][userid].push(now);
            return true;
        }
    }
};