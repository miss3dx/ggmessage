const schema = mongoose.Schema({
    user_id: String,
    register_timestamp: String,
    username: String,
    custom_username: String,
    mail: String,
    password: String,
    avatar: String,
    status: String,
    badges: Number,
    banned: {type: Boolean, default: false},
    token: String
});

module.exports = mongoose.model("User", schema);