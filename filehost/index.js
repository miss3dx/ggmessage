const express = require("express");
const fs = require("fs");
const busboy = require("connect-busboy");

const app = express();
app.use(busboy());

global.mongoose = require("mongoose");

const usermodel = require("./models/user.js");

mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connection.on("connected", ()=>{
    console.log("[FileHost] Connected.");
});

app.post("/upload", async(req, res)=>{
    if(!req.query["authorization"]){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    let user = await usermodel.findOne({token: req.query.authorization});
    if(!user){res.status(401);return res.json({error: true, message: "Unauthorized."});}
    if(user.banned){res.status(403);return res.json({error: true, message: "Banned."});}
    try {
        req.busboy.once("file", (fieldname, file, filename)=>{
            let fileid = require("crypto").Hash("sha256").update(user.username+Date.now()+filename).digest("hex")
            let fstream = fs.createWriteStream(__dirname + "/data/" + fileid+"_"+filename);
            file.pipe(fstream);
            fstream.on("close", ()=>{
                return res.json({error: false, file_id: fileid, url: process.env.FILEHOST_URL+(process.env.FILEHOST_URL == 80?"":`:${process.env.FILEHOST_PORT}`)+"/"+fileid+"_"+filename});
            });
        });
        req.pipe(req.busboy);
    }catch(e){
        let errorid = require("crypto").Hash("sha256").update(Date.now()+user.username).digest("hex");
        console.log(`[FileHost/Errors/${errorid}] ${e}`);
        return res.json({error: true, message: `Some error occured. Contact support and provide this: ${errorid}.`});
    }
});

app.get("/*", (req, res, next)=>{
    if(req.url == "/")next();
    try {
        fs.accessSync(__dirname + "/data/" + req.url.slice(1), fs.constants.F_OK);
        res.sendFile(__dirname + "/data/" + req.url.slice(1));
    }catch(e){res.status(404); return res.json({error: true, message: "Not Found."});}
});

app.use((req, res)=>{
    res.redirect(process.env.MAIN_URL + (process.env.MAIN_PORT == 80?"":process.env.MAIN_PORT));
});

app.listen(process.env.FILEHOST_PORT, ()=>{
    console.log(`[FileHost] Running on port ${process.env.FILEHOST_PORT}.`);
});